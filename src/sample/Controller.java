package sample;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;

public class Controller {
    public static int START_AMOUNT = 0;

    @FXML
    private Text money, moneyLondon, moneyManila, warsawIncome, londonIncome, manilaIncome, sumIncomeWarsaw, sumIncomeLondon, sumIncomeManila;
    @FXML
    private Tab LondonTab, ManilaTab;
    @FXML
    private Circle indicatorWarsaw, indicatorLondon, indicatorLondon2, indicatorLondon3, indicatorManila, indicatorManila2, indicatorManila3;
    @FXML
    private Button buttonLondon, buttonLondon2, buttonLondon3, buttonManila, buttonManila2, buttonManila3;
    @FXML
    private Rectangle tablesWarsaw1, tablesWarsaw3, tablesWarsaw4, tablesWarsaw5, tablesWarsaw6, tablesWarsaw7, tablesWarsaw8;
    @FXML
    private Rectangle tablesLondon1, tablesLondon2, tablesLondon3, tablesLondon4, tablesLondon5, tablesLondon6;
    @FXML
    private Rectangle tablesManila1, tablesManila2, tablesManila3, tablesManila4, tablesManila5, tablesManila6, tablesManila7, tablesManila8, tablesManila9, tablesManila10, tablesManila11, tablesManila12, tablesManila13, tablesManila14, tablesManila15, tablesManila16;


    Task<Integer> progressTask;
    Task<Integer> incomeTaskWarsaw;
    Task<Integer> incomeTaskLondon;
    Task<Integer> incomeTaskManila;
    Task<Integer> summaryIncomeTask;
    Task<Integer> moneyCheck;

    int value = 100000;
    int counter = 1;
    int income = 0;
    int periodIncomeWarsaw = 0;
    int periodIncomeLondon = 0;
    int periodIncomeManila = 0;
    int summaryIncome = periodIncomeWarsaw + periodIncomeLondon + periodIncomeManila;


    public void pressButton() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("secondWindow.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        // @TODO(1)
        // set modality
        stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(primaryStage);
        stage.setScene(new Scene(root1));
        stage.show();
    }

    public void enableButtonActionWarsaw(MouseEvent e) {
        Rectangle re = (Rectangle) e.getSource();
        re.getStyleClass().add("unlocked");
        value = value - 500;
        income++;
        periodIncomeWarsaw++;
        summaryIncome++;
    }

    public void enableButtonActionLondon(MouseEvent e) {
        Rectangle re = (Rectangle) e.getSource();
        re.getStyleClass().add("unlocked");
        value = value - 1000;
        income = income + 2;
        periodIncomeLondon = periodIncomeLondon + 2;
        summaryIncome = summaryIncome + 2;
    }

    public void enableButtonActionManila(MouseEvent e) {
        Rectangle re = (Rectangle) e.getSource();
        re.getStyleClass().add("unlocked");
        value = value - 1500;
        income = income + 3;
        periodIncomeManila = periodIncomeManila + 3;
        summaryIncome = summaryIncome + 3;
    }

    public void mouseEnterAction(MouseEvent e) {

        Tooltip tablesWarsawT = new Tooltip();
        tablesWarsawT.setText("Buy for 500$");

        Tooltip tablesLondonT = new Tooltip();
        tablesLondonT.setText("Buy for 1000$");

        Tooltip tablesManilaT = new Tooltip();
        tablesManilaT.setText("Buy for 1500$");

        Tooltip buttonLondonT = new Tooltip();
        buttonLondonT.setText("To buy the new office in London you must have 9000$");

        Tooltip buttonManilaT = new Tooltip();
        buttonManilaT.setText("To buy the new office in Manila you must have 15000$");

        // Tooltips for Warsaw tables
        Tooltip.install(tablesWarsaw1, tablesWarsawT);
        Tooltip.install(tablesWarsaw3, tablesWarsawT);
        Tooltip.install(tablesWarsaw4, tablesWarsawT);
        Tooltip.install(tablesWarsaw5, tablesWarsawT);
        Tooltip.install(tablesWarsaw6, tablesWarsawT);
        Tooltip.install(tablesWarsaw7, tablesWarsawT);
        Tooltip.install(tablesWarsaw8, tablesWarsawT);

        //Tooltip for London unlocking button
        Tooltip.install(indicatorLondon, buttonLondonT);

        //Tooltip for Manila unlocking button
        Tooltip.install(indicatorManila, buttonManilaT);
        Tooltip.install(indicatorManila2, buttonManilaT);

        // Tooltips for London tables
        Tooltip.install(tablesLondon1, tablesLondonT);
        Tooltip.install(tablesLondon2, tablesLondonT);
        Tooltip.install(tablesLondon3, tablesLondonT);
        Tooltip.install(tablesLondon4, tablesLondonT);
        Tooltip.install(tablesLondon5, tablesLondonT);
        Tooltip.install(tablesLondon6, tablesLondonT);

        // Tooltips for Manila tables
        Tooltip.install(tablesManila1, tablesManilaT);
        Tooltip.install(tablesManila2, tablesManilaT);
        Tooltip.install(tablesManila3, tablesManilaT);
        Tooltip.install(tablesManila4, tablesManilaT);
        Tooltip.install(tablesManila5, tablesManilaT);
        Tooltip.install(tablesManila6, tablesManilaT);
        Tooltip.install(tablesManila7, tablesManilaT);
        Tooltip.install(tablesManila8, tablesManilaT);
        Tooltip.install(tablesManila9, tablesManilaT);
        Tooltip.install(tablesManila10, tablesManilaT);
        Tooltip.install(tablesManila11, tablesManilaT);
        Tooltip.install(tablesManila12, tablesManilaT);
        Tooltip.install(tablesManila13, tablesManilaT);
        Tooltip.install(tablesManila14, tablesManilaT);
        Tooltip.install(tablesManila15, tablesManilaT);
        Tooltip.install(tablesManila16, tablesManilaT);
    }

    public void enableTabLondonAction() {
        LondonTab.setDisable(false);
        indicatorLondon.getStyleClass().add("turn_green");
        indicatorLondon2.getStyleClass().add("turn_green");
        indicatorLondon3.getStyleClass().add("turn_green");
        value = value - 9000;
    }

    public void enableTabManilaAction() {
        ManilaTab.setDisable(false);
        indicatorManila.getStyleClass().add("turn_green");
        indicatorManila2.getStyleClass().add("turn_green");
        indicatorManila3.getStyleClass().add("turn_green");
        value = value - 15000;
    }

    public void initialize() {
        progressTask = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                value = value + income;
                while (!isCancelled()) {
                    value = value + income;
                    updateMessage("Money: " + value + "$");
                    updateProgress(START_AMOUNT, value);
                    Thread.sleep(200);

                    //Unlocks disabling tab buttton
                    if (value >= 9000) {
                        buttonLondon.setDisable(false);
                        buttonLondon2.setDisable(false);
                        buttonLondon3.setDisable(false);

                    }
                    // Unlocks disabling tab buttton
                    if (value >= 15000) {
                        buttonManila.setDisable(false);
                        buttonManila2.setDisable(false);
                        buttonManila3.setDisable(false);
                    }
                }
                return value;
            }
        };

        incomeTaskWarsaw = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                while (!isCancelled()) {
                    // 'periodIncomeWarsaw * 180' because income is per minute
                    updateMessage("Office income per min.: ~" + periodIncomeWarsaw * 300 + "$");
                    updateProgress(START_AMOUNT, periodIncomeWarsaw * 300);
                    Thread.sleep(200);
                }
                return periodIncomeWarsaw;
            }
        };

        incomeTaskLondon = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                while (!isCancelled()) {
                    // 'periodIncomeWarsaw * 180' because income is per minute
                    updateMessage("Office income per min.: ~" + periodIncomeLondon * 300 + "$");
                    updateProgress(START_AMOUNT, periodIncomeLondon * 300);
                    Thread.sleep(200);
                }
                return periodIncomeLondon;
            }
        };

        incomeTaskManila = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                while (!isCancelled()) {
                    // 'periodIncomeWarsaw * 300' because income is per minute
                    updateMessage("Office income per min.: ~" + periodIncomeManila * 300 + "$");
                    updateProgress(START_AMOUNT, periodIncomeManila * 300);
                    Thread.sleep(200);
                }
                return periodIncomeManila;
            }
        };

        summaryIncomeTask = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                while (!isCancelled()) {
                    // 'periodIncomeWarsaw * 300' because income is per minute
                    updateMessage("Summary income per min.: ~" + summaryIncome * 300 + "$");
                    updateProgress(START_AMOUNT, summaryIncome * 300);
                    Thread.sleep(200);
                }
                return summaryIncome;
            }
        };
        // @TODO Why this task doesn't work.(!)
        moneyCheck = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                while(!isCancelled()) {
                    if(value < 0) {
                        System.out.println("elo");
                        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("secondWindow.fxml"));
                        Parent root2 = (Parent) fxmlLoader2.load();
                        Stage stage2 = new Stage();
                        stage2.setScene(new Scene(root2));
                        stage2.show();
                    }
                }
                return summaryIncome;
            }
        };

        Thread thread = new Thread(progressTask);
        Thread thread2 = new Thread(incomeTaskWarsaw);
        Thread thread3 = new Thread(incomeTaskLondon);
        Thread thread4 = new Thread(incomeTaskManila);
        Thread thread5 = new Thread(summaryIncomeTask);
        Thread thread6 = new Thread(moneyCheck);

        thread.setDaemon(true);
        thread.start();
        thread2.setDaemon(true);
        thread2.start();
        thread3.setDaemon(true);
        thread3.start();
        thread4.setDaemon(true);
        thread4.start();
        thread5.setDaemon(true);
        thread5.start();
        thread6.setDaemon(true);
        thread6.start();

        if (null != money) {
            money.textProperty().bind(progressTask.messageProperty());
            moneyLondon.textProperty().bind(progressTask.messageProperty());
            moneyManila.textProperty().bind(progressTask.messageProperty());
            warsawIncome.textProperty().bind(incomeTaskWarsaw.messageProperty());
            londonIncome.textProperty().bind(incomeTaskLondon.messageProperty());
            manilaIncome.textProperty().bind(incomeTaskManila.messageProperty());
            sumIncomeWarsaw.textProperty().bind(summaryIncomeTask.messageProperty());
            sumIncomeLondon.textProperty().bind(summaryIncomeTask.messageProperty());
            sumIncomeManila.textProperty().bind(summaryIncomeTask.messageProperty());
        }
        System.out.println(progressTask.messageProperty().getValue());
    }
}




