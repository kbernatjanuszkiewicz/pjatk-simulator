package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;




public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("ｐｊａｔｋ　ｓｉｍｕｌａｔｏｒ　芋運し");
        primaryStage.setScene(new Scene(root, 500, 450));
        primaryStage.maxWidthProperty();

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
